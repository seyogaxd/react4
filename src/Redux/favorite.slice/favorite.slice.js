import { createSlice } from '@reduxjs/toolkit';
import items from "../../../public/items.json"

const initialState = {
    favoriteItems: JSON.parse(localStorage.getItem('FavoriteItems')) || [],
};

export const favoriteSlice = createSlice({
    name: 'favorite',
    initialState,
    reducers: {
        addToFavorite: (state, action) => {
            const itemToAddID = action.payload;
            const itemToAdd = items.find(item => item.id === itemToAddID)
            const itemIdx = state.favoriteItems.findIndex(
                (item) => item.id === itemToAdd.id
            );

            if (itemIdx === -1) {
                state.favoriteItems.push(itemToAdd);
            } else {
                state.favoriteItems = state.favoriteItems.filter(
                    (item) => item.id !== itemToAdd.id
                );
            }

            localStorage.setItem(
                'FavoriteItems',
                JSON.stringify(state.favoriteItems)
            );
        },
    },
});

export const { addToFavorite } = favoriteSlice.actions;
export default favoriteSlice.reducer;
