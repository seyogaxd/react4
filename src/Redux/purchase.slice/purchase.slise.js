import { createSlice } from '@reduxjs/toolkit';
import items from "../../../public/items.json"

const initialState = {
    shopedItems: JSON.parse(localStorage.getItem('ShopItems')) || [],
};

export const purchaseSlice = createSlice({
    name: 'purchase',
    initialState,
    reducers: {
        addToShoped: (state, action) => {
            const itemToAddID = action.payload;
            const itemToAdd = items.find(item => item.id === itemToAddID)
            
            state.shopedItems.push(itemToAdd);

            localStorage.setItem(
                'ShopItems',
                JSON.stringify(state.shopedItems)
            );
        },
        deleteFromShoped: (state, action) => {
            const itemToDeleteID = action.payload;

            state.shopedItems = state.shopedItems.filter(
                (item) => item.id !== itemToDeleteID
            )

            localStorage.setItem(
                'ShopItems',
                JSON.stringify(state.shopedItems)
            );
        }

    },
});

export const { addToShoped, deleteFromShoped } = purchaseSlice.actions;
export default purchaseSlice.reducer;
