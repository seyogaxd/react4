import { createSlice } from '@reduxjs/toolkit';

export const modalSlice = createSlice({
  name: 'modal',
  initialState: {
    isModalOpen: false,
    isImageModalOpen: false,
    modalItemId: null,
  },
  reducers: {
    openModal: (state, action) => {
      state.isModalOpen = true;
      state.modalItemId = action.payload.itemId;
    },
    openImageModal: (state, action) => {
      state.isImageModalOpen = true;
      state.modalItemId = action.payload.itemId;
    },
    closeModal: (state) => {
      state.isModalOpen = false;
    },
    closeImageModal: (state) => {
      state.isImageModalOpen = false;
    },
  },
});

export const { openModal, openImageModal, closeModal, closeImageModal } = modalSlice.actions;

export default modalSlice.reducer;  