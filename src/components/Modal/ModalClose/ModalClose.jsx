import '../Styles/ModalStyles.scss'

export default function({onClick})
{
    return(
        <button className="main__modal-close-button" onClick={onClick}>
            &times;
        </button>
    )
}