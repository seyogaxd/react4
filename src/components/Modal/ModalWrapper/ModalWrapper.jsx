import '../Styles/ModalStyles.scss'
import { useDispatch } from "react-redux";
import { closeModal, closeImageModal } from "../../../Redux/modal.slice/modal.slice";
export default function ModalWrapper({children})
{
    const dispatch = useDispatch();
    function clickOutWindow(ev)
    {
        if (ev.target === ev.currentTarget) {
            dispatch(closeImageModal());
            dispatch(closeModal());
        }
    }

    return(
        <div className="main__modal-wrapper" onClick={clickOutWindow}>
            {children}
        </div>
    )
}