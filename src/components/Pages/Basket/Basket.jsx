
import ShopItem from "../../Index/Main/ShopItem/ShopItem";
import BasketMain from "../Basket/Main/BasketMain"
import ModalImage from "../../Modal/ModalTypes/ModalImage"

import { useSelector } from "react-redux";

export default function BasketPage(){
    
    const isImageModalOpen = useSelector(state => state.modal.isImageModalOpen);
    return(
        <>
            <BasketMain/>
            {isImageModalOpen&&(
                 <ModalImage/>
            )}
        </>
        
    )
}